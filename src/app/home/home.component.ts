import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  types = [
    {
      title: 'Bungalows',
      values: ['Lonavala', 'Igatpuri', 'Alibaug', 'Karjat', 'Ahmedabad'],
    },
    {
      title: 'Cottages',
      values: ['Manali', 'Mussorie', 'Shimla', 'Coorg', 'Wayanad'],
    },
    {
      title: 'Luxury Villas',
      values: ['Bangalore', 'kochi', 'Goa', 'Chennai', 'Kasauli'],
    },
    {
      title: 'Homestay',
      values: ['Ooty', 'Kodaikanal', 'Jaipur', 'Udaipur', 'Gangtok'],
    },
  ];
  constructor(private router: Router) {}

  ngOnInit(): void {}

  navigateToOccasions() {
    console.log('occasions');

    this.router.navigate(['occasions']);
  }

  navigateToOffsites() {
    console.log('offsites');
    this.router.navigate(['offsites']);
  }
}
