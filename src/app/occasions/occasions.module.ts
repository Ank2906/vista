import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OccasionsRoutingModule } from './occasions-routing.module';
import { OccasionsComponent } from './occasions.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [OccasionsComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    OccasionsRoutingModule,
  ],
})
export class OccasionsModule {}
