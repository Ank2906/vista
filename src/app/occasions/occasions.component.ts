import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-occasions',
  templateUrl: './occasions.component.html',
  styleUrls: ['./occasions.component.scss'],
})
export class OccasionsComponent implements OnInit {
  types = [
    {
      title: 'Bungalows',
      values: ['Lonavala', 'Igatpuri', 'Alibaug', 'Karjat', 'Ahmedabad'],
    },
    {
      title: 'Cottages',
      values: ['Manali', 'Mussorie', 'Shimla', 'Coorg', 'Wayanad'],
    },
    {
      title: 'Luxury Villas',
      values: ['Bangalore', 'kochi', 'Goa', 'Chennai', 'Kasauli'],
    },
    {
      title: 'Homestay',
      values: ['Ooty', 'Kodaikanal', 'Jaipur', 'Udaipur', 'Gangtok'],
    },
  ];

  events = [
    'Mask Group 32.svg',
    'Mask Group 34.svg',
    'Mask Group 33.svg',
    'Mask Group 29.svg',
    'Mask Group 28.svg',
  ];

  stores = [
    'Personalized Invite',
    'Custom Zoom Background',
    'Personalized Games & Entertainment',
    'Live Music or Karaoke',
    'Birthday Bingo',
    'Professional Host',
  ];

  services = [
    {
      img: 'Mask Group 35.svg',
      heading: 'Video Editing',
      subHeading:
        'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui official Excepteur sint occaecat cupidatat non',
    },
    {
      img: 'Mask Group 36.svg',
      heading: 'Food',
      subHeading:
        'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui official Excepteur sint occaecat cupidatat non',
    },
    {
      img: 'Mask Group 37.svg',
      heading: 'Decorations',
      subHeading:
        'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui official Excepteur sint occaecat cupidatat non',
    },
    {
      img: 'Mask Group 38.svg',
      heading: 'Live Singer',
      subHeading:
        'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui official Excepteur sint occaecat cupidatat non',
    },
  ];

  enquiryForm: FormGroup;

  constructor() {}

  ngOnInit(): void {
    this.enquiryForm = new FormGroup({
      name: new FormControl('', Validators.required),
      number: new FormControl(null, [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(10),
      ]),
      email: new FormControl('', [Validators.required, Validators.email]),
      groupSize: new FormControl(''),
      location: new FormControl(''),
      timeSlot: new FormControl(''),
    });
  }

  enquire() {
    console.log(this.enquiryForm);
    console.log(this.enquiryForm.value);
  }
}
